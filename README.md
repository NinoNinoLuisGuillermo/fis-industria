## Industria 4.0
```plantuml
@startmindmap
+[#White] Industria 4.0
**_ es
+++ La Cuarta Revolución Industrial, también conocida como industria 4.0, está cambiando la forma \n en que los negocios operan y por lo tanto, los entornos en los se ven obligados a competir
++++ involucra tecnologías
*****_ como 
++++++[#Pink] la robotica 
++++++[#Pink] Big data
++++++[#Pink] Cloud computing 
++++++[#Pink] Sistema Autónomo 
++++++[#Pink] loT
++++++[#Pink] Inteligencia Artificial 
++++ Su proposito es que en el futuro haya menos intervensión humana en los trabajos 
++++ La industria 4.0 genera grandes cambios 
*****_ como lo son
++++++ La integración de TICs en la industria de manufactura y servicios
*******_ por ejemplo 
++++++++ los almacenes de amazon donde son ordenados por robots 
+++++++++ Pero tiene sus efectos como lo son: \n -la reduccion de puestos de trabajo\n -aparicion de nuevas profesiones
++++++ Las empresas de manufactura se vuelven empresas de TICs
*******_ por ejemplo
++++++++ Los carros modernos que se ofrecen actualmente al publico terminan siendo computadoras
++++++ Nuevos paradigmas y tecnologías 
*******_ por ejemplo
++++++++ Las empresas que no se adaptan a los rapidos cambios tecnologicos están destinados a desaparecer así como lo fueron
+++++++++ blockbuster \n Nokia \n Entre otras 
++++++++++ Al no tomar la debida importancia al avance tecnologico estás empresar poco a poco fueron desapareciendo al punto\n de que la gente no suele usar más sus productos o en su debido caso, desaparecieron
++++ Trae consigo nuevas cosas
*****_ Como lo son 
++++++ Nuevos trabajos 
*******_ Como
++++++++ Desarrolladores de software 
++++++++ Gamers
++++++++ Youtubers
++++++++ Gente relacionada con las redes sociales
++++ Es importante en la actualidad ya que 
+++++ La tecnología nueva facilita a los seres humanos la forma de vivir 
+++++ Se usa la tecnología de manera habitual en nuestra vida diaria 
+++++ Se crean Nuevos trabajos por lo que da nuevas oportunidades a las personas
++++ Así como no regala nuevas tecnologías 
*****_ como lo son 
++++++ la realidad aumentada
++++++ los taxis que se manejan de manera autonoma
++++++ Comunicaciones más veloces 
@endmindmap
```

## México y la industria 4.0

```plantuml
@startmindmap
+ México y la industria 4.0
++ México a progresado con la industri 4.0 teniendo varios  ejemplos de esto
***_ como
++++ el ingeniero Ernesto Rodriguez \n creo un exoesqueleto que ayuda a recuperar la movilidad perdida a la personas, además de conectar al medico con el paciente ayudando \n a las personas a sobrellevar\n de una mejor manera su rehabilitación  con su empresa we are robot 
++++ La empresa sin llaves que se está encargando de realizar una aplicación con la cual no será necesario seguir ocupando las lleves fisicas \n si no podrémos ocupas nuestros dispositivos moviles para lograr todo esto 
++ Hay empresas en México que tienen equipos antiguos y equipos modernos\n por lo que eso es un gran problema y un obstaculo en nuestro país 
***_ por lo que
++++ Se necesitaría apoyar más a la parte de crecimiento tecnologíco para que el país pueda seguir creciendo y adaptandose a nuevas tecnologías 
++ Las empresas están comenzando a implementar  la inteligencia artificial en sus empresas y México no se queda atras, las empresas están comenzando a utlizarlo \n ya que incrementa su eficacia 
++ México necesitaría de mejores recursos 
***_ como lo son 
++++ Mejores centros de trabajo 
++++ Equipos modernos con los que puedan trabajar 
++++ mayor apoyo a los ingenieros que se dedican a estudíar las tecnologías 
++++ Incrementar el apoyo a este avence para abrir nuevas oportunidades de trabajo y que se puede progresar de una manera \n más eficaz 
@endmindmap
```
